package ffhs_integration_group;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.Test;

/**
 * @author Yvo Broennimann, Giancarlo Bergamin
 * Test Klasse für die Surface Klasse. Es werden vorallem die Hilfsmethoden getestet, die 
 * die Koordinaten der Figuren veraendern, da die paintComponent Methode schlecht getestet werden kann
 */
public class SurfaceTest {

	/**
	 * Testet die CheckFinished Methode mit Reflections. Die Methode überprüft ob die zu testende Methode den Boolean 
	 * gamestart auf false setzt wenn ein Spieler 5 Punkte erreicht
	 */
	@Test
	public void testCheckFinished() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field scoreText = surface.getClass().getDeclaredField("scoreText");
			Field gameStart = surface.getClass().getDeclaredField("gameStart");
			Field scorePlayerRight = surface.getClass().getDeclaredField("scorePlayerRight");
			gameStart.setAccessible(true);
			scorePlayerRight.setAccessible(true);
			scoreText.setAccessible(true);
			scorePlayerRight.set(surface, 5);
			surface.checkFinished();
			boolean afterGameStart = (boolean) gameStart.get(surface);
			String afterScoreText = (String) scoreText.get(surface);
			assertEquals(afterGameStart, false);
			assertEquals(afterScoreText, "         Player Right Won!");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Testet die checkRestart Methode mit Reflections. Methode schaut ob der Ball auf die richtige Position
	 * gesetzt wird, wenn die Bedingungen erfüllt sind (restart boolean = true)
	 */
	@Test
	public void testCheckRestart() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field restart = surface.getClass().getDeclaredField("restart");
			Field ballY = surface.getClass().getDeclaredField("ballY");
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field ballDirectionX = surface.getClass().getDeclaredField("ballDirectionX");
			Field ballDirectionY = surface.getClass().getDeclaredField("ballDirectionY");
			Field ballDiameter = surface.getClass().getDeclaredField("ballDiameter");
			
			ballX.setAccessible(true);
			ballY.setAccessible(true);
			ballDirectionX.setAccessible(true);
			ballDirectionY.setAccessible(true);
			restart.setAccessible(true);
			ballDiameter.setAccessible(true);
			restart.set(surface, true);
			ballDiameter.set(surface, 20);
			surface.moveBall(5, 5);
			surface.checkRestart();
			int afterBallX = (int) ballX.get(surface);
			int afterBallY = (int) ballY.get(surface);
			
			
			assertEquals(afterBallX, 230);
			assertEquals(afterBallY, 250);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Testet die checkPlayerMovements Methode mit Reflections. Schaut ob ein Boolean im actions boolean Array
	 * auf true gesetzt ist und aendert dementsprechend die Koordinaten der Spielfiguren.
	 */
	@Test
	public void testCheckPlayerMovements() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field actions = surface.getClass().getDeclaredField("actions");
			Field playerAY = surface.getClass().getDeclaredField("playerAY");
			
			actions.setAccessible(true);
			playerAY.setAccessible(true);
			boolean[] temp = {true, false, false, false};
			actions.set(surface, temp);
			playerAY.set(surface, 6);
			surface.checkPlayerMovements();
			int afterPlayerAY = (int) playerAY.get(surface);
			
			assertEquals(3, afterPlayerAY);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test die hitPlayerLeftWall Methode mit Reflections. Es würd überprüft ob das restart Flag richtig gesetzt
	 * wird wenn der Ball auf der Linken Seite ist und von keinem Spieler abgefangen wird.
	 */
	@Test
	public void testHitPlayerLeftWall() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field restart = surface.getClass().getDeclaredField("restart");
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field scorePlayerRight = surface.getClass().getDeclaredField("scorePlayerRight");
			ballX.setAccessible(true);
			scorePlayerRight.setAccessible(true);
			restart.setAccessible(true);
			ballX.set(surface, 0);
			int beforeScorePlayerRight = (int) scorePlayerRight.get(surface);
			surface.hitPlayerLeftWall();
			boolean afterRestart = (boolean) restart.get(surface);
			int afterScorePlayerRight = (int) scorePlayerRight.get(surface);
			
			assertEquals(afterRestart, true);
			assertEquals(beforeScorePlayerRight+1, afterScorePlayerRight);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Testet die hitPlayerRightWall Methode mit Reflections. Es würd überprüft ob das restart Flag richtig gesetzt
	 * wird wenn der Ball auf der rechten Seite ist und von keinem Spieler abgefangen wird.
	 */
	@Test
	public void testHitPlayerRightWall() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field restart = surface.getClass().getDeclaredField("restart");
			Field ballDiameter = surface.getClass().getDeclaredField("ballDiameter");
			Field scorePlayerLeft = surface.getClass().getDeclaredField("scorePlayerLeft");
			ballX.setAccessible(true);
			restart.setAccessible(true);
			scorePlayerLeft.setAccessible(true);
			ballDiameter.setAccessible(true);
			ballX.set(surface, 600);
			ballDiameter.set(surface, 20);
			int beforeScorePlayerLeft = (int) scorePlayerLeft.get(surface);
			surface.hitPlayerRightWall();
			boolean afterRestart = (boolean) restart.get(surface);
			int afterScorePlayerLeft = (int) scorePlayerLeft.get(surface);
			
			assertEquals(afterRestart, true);
			assertEquals(beforeScorePlayerLeft+1, afterScorePlayerLeft);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Testet die hitRegularWall Methode mit Reflections. Dabei wird geschaut ob die Ball Richtung richtig verändert wird,
	 * wenn der Ball die obere oder untere Wand beruehrt.
	 */
	@Test
	public void testHitRegularWall() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field ballY = surface.getClass().getDeclaredField("ballY");
			Field height = surface.getClass().getDeclaredField("height");
			Field ballDiameter = surface.getClass().getDeclaredField("ballDiameter");
			Field ballDirectionY = surface.getClass().getDeclaredField("ballDirectionY");
			height.setAccessible(true);
			ballY.setAccessible(true);
			ballDirectionY.setAccessible(true);
			ballDiameter.setAccessible(true);
			ballY.set(surface, 0);
			int beforeX = (int) ballDirectionY.get(surface);
			surface.hitRegularWall();
			int afterX = (int) ballDirectionY.get(surface);
			
			assertEquals(beforeX*-1, afterX);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Testet die hitPlayerLeft Methode mit Reflections. Es wird geschaut ob die Richtung des Balls richtig veraendert wird,
	 * wenn er den linken Spieler beruehrt.
	 */
	@Test
	public void testHitPlayerLeft() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field ballY = surface.getClass().getDeclaredField("ballY");
			Field playerAY = surface.getClass().getDeclaredField("playerAY");
			Field ballDirectionX = surface.getClass().getDeclaredField("ballDirectionX");
			ballX.setAccessible(true);
			ballY.setAccessible(true);
			playerAY.setAccessible(true);
			playerAY.set(surface, 200);
			ballDirectionX.setAccessible(true);
			ballX.set(surface, 0);
			ballY.set(surface, 300);
			int beforeX = (int) ballDirectionX.get(surface);
			surface.hitPlayerLeft();
			int afterX = (int) ballDirectionX.get(surface);
			
			assertEquals(beforeX*-1, afterX);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Testet die hitPlayerRight Methode mit Reflections. Es wird geschaut ob die Richtung des Balls richtig veraendert wird,
	 * wenn er den rechten Spieler beruehrt.
	 */
	@Test
	public void testHitPlayerRight() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field ballY = surface.getClass().getDeclaredField("ballY");
			Field playerBY = surface.getClass().getDeclaredField("playerBY");
			Field ballDirectionX = surface.getClass().getDeclaredField("ballDirectionX");
			ballX.setAccessible(true);
			ballY.setAccessible(true);
			playerBY.setAccessible(true);
			playerBY.set(surface, 200);
			ballDirectionX.setAccessible(true);
			ballX.set(surface, 501);
			ballY.set(surface, 200);
			int beforeX = (int) ballDirectionX.get(surface);
			surface.hitPlayerRight();
			int afterX = (int) ballDirectionX.get(surface);
			
			assertEquals(beforeX*-1, afterX);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Testet die Methode moveBall mit Reflections. Es wird geschaut ob die Koordinaten richtig veraendert werden.
	 */
	@Test
	public void testMoveBall() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		try {
			Field ballX = surface.getClass().getDeclaredField("ballX");
			Field ballY = surface.getClass().getDeclaredField("ballY");
			ballX.setAccessible(true);
			ballY.setAccessible(true);
			int beforeX = (int) ballX.get(surface);
			int beforeY = (int) ballY.get(surface);
			surface.moveBall(5, 5);
			int afterX = (int) ballX.get(surface);
			int afterY = (int) ballY.get(surface);
			
			assertEquals(beforeX+5, afterX);
			assertEquals(beforeY+5, afterY);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Testet den Getter getActions. Es wird geschaut ob ein Array mit der Laenge 4 zurueck gegeben wird.
	 */
	@Test
	public void testGetActions() {
		Surface surface = new Surface();
		surface.setHeight(500);
		surface.setWidth(500);
		surface.setPaneSize();
		boolean[] actions = new boolean[4];
		assertEquals(surface.getActions().length, actions.length);
	}

}
