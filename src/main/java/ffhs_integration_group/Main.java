package ffhs_integration_group;

import javax.swing.JFrame;

/**
 * @author Yvo Broennimann, Giancarlo Bergamin
 * Main Klasse des Spiel. Erzeugt ein Frame mit einem Spielfeld und fuegt ein keylistener hinzu.
 *
 */
public class Main {

	/**
	 * Main Methode des Spiels. Es wird ein neues Surface Objekt erzeugt und in ein JFrame Objekt eingebettet.
	 * Hier wird auch der eventHandler erzeugt und dem Frame hinzugefügt
	 * @param args
	 */
	public static void main(String[] args) {

		int windowHeigth = 500;
		int windowWidth = 500;
		
		JFrame frame = new JFrame();
		frame.setSize(windowHeigth, windowWidth);
		
		Surface gameSurface = new Surface();
		frame.add(gameSurface);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addKeyListener(new EventHandler(gameSurface));
		frame.setVisible(true);
		gameSurface.setHeight(frame.getContentPane().getSize().getHeight()); //diese drei Methodenaufrufe erlauben die richtige Anzeige (auch auf Windows)
		gameSurface.setWidth(frame.getContentPane().getSize().getWidth());
		gameSurface.setPaneSize();
	}
}
