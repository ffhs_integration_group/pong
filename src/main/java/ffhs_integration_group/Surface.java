package ffhs_integration_group;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JComponent;
import javax.swing.Timer;
/**
 * 
 * @author Giancarlo Bergamin, Yvo Broennimann
 * Klasse, welche die Hauptbestandteile der Mechanik des Spiels beinhaltet. In dieser Klasse werden
 * die GUI Komponenten (Schläger, Ball, Score, Trennlinie etc.) gezeichnet und bewegt. Diese Klasse enthält
 * auch Methoden, welche Ueberpruefen, ob der Ball eine Wand oder einen Schläger berührt.
 *
 */
public class Surface extends JComponent {

	public static final int PLAYER_A_UP = 0;
	public static final int PLAYER_A_DOWN = 1;
	public static final int PLAYER_B_UP = 2;
	public static final int PLAYER_B_DOWN = 3;

	private static final long serialVersionUID = 1L;

	private boolean restart = false;	// restart Flag
	private boolean gameStart = false;	// gameStart Flag

	private int height;		// Höhe des Windows
	private int width;		// Breite des Windows

	private int playerAY;	// Y-Koordinate des Player A
	private int playerBY;	// Y-Koordinate des Player B

	private int playerAX;	// X-Koordinate des Player A
	private int playerBX;	// X-Koordinate des Player B

	private int ballX;		// X-Koordinate des Balls
	private int ballY;		// Y-Koordinate des Balls

	private int playerWidth;	// Breite des Schlägers
	private int playerHeight;	// Höhe des Schlägers

	private int ballDirectionX;	// X-Komponente der Geschwindigkeit des Balls
	private int ballDirectionY;	// Y-Komponente der Geschwindigkeit des Balls

	private int ballDiameter;	// Den Durchmessser des Balls

	private int scorePlayerLeft = 0;	// Punkte des Linken Spielers
	private int scorePlayerRight = 0;	// Punkte des Rechten Spielers

	private int halfOfSurfaceX;	
	private int halfOfSurfaceY;

	private String scoreText;	// Text des Scores
	private Font scoreFont;		// Schriftart des Textes

	private BasicStroke dashed;	

	private Timer timer;		// Swing-Timer

	private boolean[] actions = new boolean[4];
	
	private int[] possibleBallDirections;		// Array von integer Nummern

	/**
	 * Konstruktor der Klasse Surface. Es werden diverse Parameter initalisiert, wie zum Beispiel
	 * die Hoehe und Breite der Schlaeger, die Groesse des Balles, die Geschwindigkeit des Balles,
	 * etc. Zudem wird ein Swing-Timer initalisiert, welcher alle 9 Millisekunden die repaint() Funktion aufruft.
	 * Dem Konstruktor werden die Hoehe und Breite des Frames uebergeben, damit man die Spielkomponenten der
	 * Groesse des Frames anpassen kann.
	 * 
	 * @param width	Breite des Frames
	 * @param height Hoehe des Frames
	 */
	public Surface() {
		super();
		
		possibleBallDirections = new int[4];	// Array der Werte, welche eine Komponente der Geschwindigkeit des Balles annehmen kann
		possibleBallDirections[0] = -2;
		possibleBallDirections[1] = -1;
		possibleBallDirections[2] = 1;
		possibleBallDirections[3] = 2;
		
		int random = ThreadLocalRandom.current().nextInt(0, 4); // Generiert zufaellig eine Zahl zwischen 0 und 3 (inklusive)
		
		ballDirectionX = possibleBallDirections[random];			// es wird eine zufaellige Geschwindigkeit genommen
		ballDirectionY = possibleBallDirections[(random + 2) % 4];	// hier muss mir dieser Formel gerechnet werden, damit nicht beide Geschwindigkeitskomponenten gleich sind (z.B vx:2, vy:2), sonst wuerde sich der Ball immer auf der selben Bahn hin und her bewegen

		float[] dashes = new float[] { 15f, 5f, 5f, 5f };	// Definiert das Aussehen der Trennlinie
		dashed = new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, dashes, 0);

		timer = new Timer(9, new ActionListener() {		// Swing-Timer Initialisierung
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		});
	}

	
	/**
	 * Wenn das gameStart Flag auf True gesetzt ist, werden diverse Methoden aufgerufen.
	 * Danach werden die Spielkomponenten gezeichnet. Alle 9 Millisekunden werden dann diese
	 * Methoden wieder aufgerufen (Swing-Timer ruft repaint() auf)
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (gameStart) {
			checkRestart();					// Falls das restart Flag auf true gesetzt ist wird der Ball in die Mitte versetzt und die Geschwindigkeit wird neu zufaellig erzeugt
			checkPlayerMovements();			// Ueberprueft ob die "Bewegungsflags" gesetzt sind und passt dann gegebenenfalls die Schlaegerpositionen an
			hitPlayerLeftWall();			// Ueberprueft ob der Ball die linke Spielerwand getroffen hat, wenn ja, dann wird der Score angepasst und das restart Flag auf true gesetzt
			hitPlayerRightWall();			// Ueberprueft ob der Ball die rechte Spielerwand getroffen hat, wenn ja, dann wird der Score angepasst und das restart Flag auf true gesetzt
			hitRegularWall();				// Ueberprieft ob der Ball eine der beiden horizontalen Waende getroffen hat, wenn ja wird die y-Geschwindigkeitskomponente mit -1 multipliziert
			hitPlayerLeft();				// Ueberprueft ob der Ball den linken Schlager getroffen hat, wenn ja wird die x-Geschwindigkeitskomponente mit -1 multipliziert
			hitPlayerRight();				// Ueberprueft ob der Ball den rechten Schlager getroffen hat, wenn ja wird die x-Geschwindigkeitskomponente mit -1 multipliziert
			moveBall(ballDirectionX, ballDirectionY);	// Die Koordinaten des Balls werden angepasst 
			checkFinished();				// Ueberprueft ob das Spiel von einem Spieler gewonnen wurde, wenn ja wird das gameStart Flag auf false gesetzt und eine Gewinnermassage wird ausgegeben
		}

		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(new Color(80, 80, 80));	// Setzt eine neue Farbe
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);	// Setzt Renderoptionen (zeichnet Ball etwas schoener)
		g2d.setFont(scoreFont);					// Setzt die Schriftart
		g2d.drawString(scoreText, 210, 20);		// Zeichnet den ScoreText
		g2d.fillRect(playerAX, playerAY, playerWidth, playerHeight);	// Zeichnet den linken Schlaeger
		g2d.fillRect(playerBX, playerBY, playerWidth, playerHeight);	// Zeichnet den rechten Schlaeger
		g2d.fillOval(ballX, ballY, ballDiameter, ballDiameter); 		// Zeichnet den Ball

		g2d.setStroke(dashed);		// Passt die Art der Trennlinie an
		g2d.drawLine(halfOfSurfaceX, 0, halfOfSurfaceX, height); 		// Zeichnet die Trennlinie
	}

	
	/**
	 * Diese Methode startet das Spiel (der timer wird gestartet, der Score wird als 0 : 0 angezeigt). zudem wird
	 * das gameStart Flag auf true gesetzt.
	 */
	public void start() {
		gameStart = true;
		scoreText = "0          0";
		timer.start();
	}
	
	/**
	 * Setzt alle nötigen Längen und andere Variablen abhängig von der Spielfeldgroesse
	 */
	public void setPaneSize(){
		halfOfSurfaceX = width / 2;
		halfOfSurfaceY = height / 2;
		scoreText = "        Press SPACE to Start";
		scoreFont = new Font("Arial", Font.PLAIN, 20);
		playerWidth = (int) Math.round(0.02 * width);
		playerHeight = (int) Math.round(0.2 * height);
		ballDiameter = (int) Math.round(0.05 * height);
		playerAX = 0;
		playerBX = width - playerWidth;
		ballX = width / 2;	// Die Anfangskoordinaten des Balls sind in der Mitte
		ballY = height / 2;
	}

	
	/**
	 * Ueberpueft, ob ein Spieler gewonnen hat (Ist der Fall wenn einer der beiden Spieler 5 Punkte
	 * erreicht hat). Das gameStart Flag wird dann auf false gesetzt und es wird die Gewinnermassage ausgegeben.
	 */
	public void checkFinished() {
		if (scorePlayerRight == 5) {
			gameStart = false;
			scoreText = "         Player Right Won!";
		}
		if (scorePlayerLeft == 5) {
			gameStart = false;
			scoreText = "         Player Left Won!";
		}
	}
	
	/**
	 * Wenn das restart Flag auf true gesetzt ist (was der Fall ist, falls der Ball eine der beiden Spielerwaende berührt)
	 * werden die Ball Koordinaten wieder in die Mitte des Spielfelds gesetzt und die Geschwindikeitskomponenten werden nochmals
	 * zufaellig generiert. Danach wir das restart Flag wieder auf false gesetzt.
	 */
	public void checkRestart() {
		if (restart) {
			
			int random = ThreadLocalRandom.current().nextInt(0, 4);
			
			ballX = halfOfSurfaceX - ballDiameter;
			ballY = halfOfSurfaceY;
			ballDirectionX = possibleBallDirections[random];
			ballDirectionY = possibleBallDirections[(random + 2) % 4];
		}
		restart = false;
	}
	
	/**
	 *  In dieser Methode werden die MovementFlags ueberprueft und gegebenenfalls die Schlagerepositionen angepasst.
	 */
	public void checkPlayerMovements() {
		if (actions[PLAYER_A_UP]) {
			if (playerAY > 0) {
				playerAY -= 3;
			}
		}

		if (actions[PLAYER_A_DOWN]) {
			if (playerAY <= (height - playerHeight)) {
				playerAY += 3;
			}
		}

		if (actions[PLAYER_B_UP]) {
			if (playerBY > 0) {
				playerBY -= 3;
			}
		}

		if (actions[PLAYER_B_DOWN]) {
			if (playerBY <= (height - playerHeight)) {
				playerBY += 3;
			}
		}
	}

	
	/**
	 * Diese Methode ueberprueft, ob der Ball die linke Spielerwand getroffen hat. Falls dies der Fall ist
	 * wird der Score des rechten Spielers erhoeht und das restart Flag wird auf true gesetzt.
	 */
	public void hitPlayerLeftWall() {
		if (ballX <= 0) {
			scorePlayerRight++;
			restart = true;
		}

	}
	
	/**
	 * Diese Methode ueberprueft, ob der Ball die rechte Spielerwand getroffen hat. Falls dies der Fall ist
	 * wird der Score des linken Spielers erhoeht und das restart Flag wird auf true gesetzt.
	 */
	public void hitPlayerRightWall() {
		if (ballX >= width - ballDiameter) {
			scorePlayerLeft++;
			restart = true;
		}

	}
	
	/**
	 * Diese Methode ueberprueft ob der Ball eine der beiden horizontalen Waende beruehrt hat, falls dies der Fall
	 * ist wir die y-Geschwindigkeitskomponente des Balls mit -1 multipliziert.
	 */
	public void hitRegularWall() {
		if (ballY <= 0 || ballY >= height - ballDiameter) {
			ballDirectionY *= -1;
		}

	}

	/**
	 * Diese Methode ueberprueft, ob der Ball den linken Schlaeger getroffen hat, wenn ja wird die x-Geschwindigkeitskomponente
	 * des Balls mit -1 multipliziert
	 */
	public void hitPlayerLeft() {
		if (ballX <= playerWidth && (playerAY <= ballY) && (ballY <= (playerAY + playerHeight))) {
			ballDirectionX *= -1;
		}

	}

	/**
	 * Diese Methode ueberprueft, ob der Ball den rechten Schlaeger getroffen hat, wenn ja wird die x-Geschwindigkeitskomponente
	 * des Balls mit -1 multipliziert
	 */
	public void hitPlayerRight() {
		if ((ballX >= width - ballDiameter - playerWidth) && (playerBY <= ballY)
				&& (ballY <= (playerBY + playerHeight))) {
			ballDirectionX *= -1;
		}

	}

	/**
	 * Diese Methode passt die Position des Balls mit einer gegebenen Geschwindigkeit an.
	 * @param x x-Komponente der Geschwindigkeits des Balls
	 * @param y y-Komponente der Geschwindigkeits des Balls
	 */
	public void moveBall(int x, int y) {
		ballX += x;
		ballY += y;
		scoreText = scorePlayerLeft + "          " + scorePlayerRight;
	}

	/**
	 * Getter Methode für die Actions.
	 * @return Array von Boolean Werten.
	 */
	public boolean[] getActions() {
		return actions;
	}
	
	/**
	 * Setzt die Hoehe des "Canvas" (nicht des gesamten windows)
	 * @param i
	 */
	public void setHeight(double i){
		height = (int) i;
	}
	
	/**
	 * Setzt die Breite des "Canvas" (nicht des gesamten windows)
	 * @param i
	 */
	public void setWidth(double i){
		width = (int) i;
	}
}
