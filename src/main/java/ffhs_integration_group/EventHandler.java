package ffhs_integration_group;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author Yvo Broennimann Giancarlo Bergamin
 * Ausgelagerter Key Listener um die Eingaben der Spieler zu verarbeiten
 *
 */
public class EventHandler implements KeyListener {
	
	private Surface surface;
		
	/**
	 * @param surface
	 * Man gibt dem EventHandler ein Surface Objekt mit, da er Datenfelder des Objekts verändert
	 */
	public EventHandler(Surface surface) {
			this.surface = surface;
		}

		/* Muss implementiert werden wegen dem Interface, macht aber nichts
		 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
		 */
		@Override
		public void keyTyped(KeyEvent e) {
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
		 * Wenn die Taste losgelassen wird wird das Flag für die Bewegung im Surface Objekt auf false gesetzt, wodurch
		 * sich der Schlaeger aufhört zu bewegen
		 */
		@Override
		public void keyReleased(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_ESCAPE:
				surface.getActions()[Surface.PLAYER_A_UP] = false;
				break;
			case KeyEvent.VK_SHIFT:
				surface.getActions()[Surface.PLAYER_A_DOWN] = false;
				break;
			case KeyEvent.VK_UP:
				surface.getActions()[Surface.PLAYER_B_UP] = false;
				break;
			case KeyEvent.VK_DOWN:
				surface.getActions()[Surface.PLAYER_B_DOWN] = false;
				break;
			}
		}

		/* (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 * Setzt ein Boolean im Booleanarray actions im Surface objekt auf true, wodurch sich der Schlaeger anfaengt zu bewegen. Ausserdem wird 
		 * bei Druck auf die Lehrtaste das Spiel am Anfang gestartet.
		 */
		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_ESCAPE:
				if (!surface.getActions()[Surface.PLAYER_A_UP])
					surface.getActions()[Surface.PLAYER_A_UP] = true;
				break;
			case KeyEvent.VK_SHIFT:
				if (!surface.getActions()[Surface.PLAYER_A_DOWN])
					surface.getActions()[Surface.PLAYER_A_DOWN] = true;
				break;
			case KeyEvent.VK_UP:
				if (!surface.getActions()[Surface.PLAYER_B_UP])
					surface.getActions()[Surface.PLAYER_B_UP] = true;
				break;
			case KeyEvent.VK_DOWN:
				if (!surface.getActions()[Surface.PLAYER_B_DOWN])
					surface.getActions()[Surface.PLAYER_B_DOWN] = true;
				break;
			case KeyEvent.VK_SPACE:
				surface.start();
			}
		}
	}